const commando = require('discord.js-commando');
const validator = require('validator');
const imagePath = '././images/branatron_flex_tape.png';

class CoinFlipCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_9gag',
            group: 'simple',
            memberName: 'bruh_9gag',
            description: 'Pulls something funny from 9gag'
        });
    }

    async run(message, args) {
        message.channel.send({files:[imagePath]});
    }
}

module.exports = CoinFlipCommand;