const commando = require('discord.js-commando');
const validator = require('validator');
const folderPath = '././images/';
let paths = ["hang.jpg", "justkms.jpg", "pills.jpg", "toaster.png", "neck.jpg", "suffocate.jpg",
             ".hangTechnique.jpg", "jump.jpg", "onePill.jpg", "train.jpg", "kurt.jpg", "marge.jpg",
            "vein.jpg"];
let messages = ["How about this way?", "This will work", "There you go", "See ya in heaven", "Hope this helped"];

for(i = 0; i < paths.length; i++) {
    paths[i] = folderPath.concat(paths[i]);
}
class KMSCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_kms',
            group: 'simple',
            memberName: 'bruh_kms',
            description: 'Telling you how to kms. params: {}'
        });
    }

    async run(message, args) {
        let generateRandomImage = Math.floor(Math.random() * paths.length);
        let generateRandomMessage = Math.floor(Math.random() * messages.length);
        message.channel.send(messages[generateRandomMessage], {files:[paths[generateRandomImage]]});
    }
}

module.exports = KMSCommand;