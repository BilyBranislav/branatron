const commando = require('discord.js-commando');
const validator = require('validator');
const gifPath = '././gifs/dna.gif';

class DNACommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_dna',
            group: 'simple',
            memberName: 'bruh_dna',
            description: 'Shows your dna. params: lenght of dna'
        });
    }

    async run(message, args) {
        if(validator.isInt(args, [1, 5])) {
            for( i = 0; i < args; i++) {
                message.channel.send({files:[gifPath]});
            }
        } else {
            message.reply('Sorry, but number of dna is rescricted 1 - 5');
        }
    }
}

module.exports = DNACommand;