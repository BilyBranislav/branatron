const commando = require('discord.js-commando');
const validator = require('validator');
const stucker = '@|<--->|#4361';
const smalo = '@ULTRAPENETRATOR#5367'

class IsGayCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_isgay',
            group: 'simple',
            memberName: 'bruh_isgay',
            description: 'Tells if you or tagged user is gay. params: [tagged user]'
        });
    }

    async run(message, args) {
        let gayNess = calculateGayness(100);
        if(args != '') {
            let names = args.split(" "); // If there are more than users tagged, writes only the first one
            if(validator.equals(names[0], stucker)) {
                gayNess = calculateGayness(10) + 90;
            } else if(validator.equals(names[0], smalo)) {
                gayNess = calculateGayness(5);
            }
            message.channel.send(names[0] + ' is gay on ' + gayNess + '% :gay_pride_flag:');
        } else {
            switch(message.author) {
                case stucker: {
                    gayNess = calculateGayness(10) + 90;
                    break;
                }
                case smalo: {
                    gayNess = calculateGayness(5);
                    break;
                }
                default: {
                    gayNess = calculateGayness(100);
                    break;
                }
            }
            message.channel.send(message.author + ' is gay on ' + gayNess + '% :gay_pride_flag:');
        }
    }
}

function calculateGayness(maxGayness) {
    return Math.floor(Math.random() * maxGayness + 1);
}

module.exports = IsGayCommand;