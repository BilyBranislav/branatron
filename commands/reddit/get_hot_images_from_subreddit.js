const commando = require('discord.js-commando');
const read_json = require('read_json.js');
const read_arguments = require('read_arguments');

class getHotImageFromReddit extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_reddit_hot',
            group: 'reddit',
            memberName: 'bruh_reddit_hot',
            description: 'Gets hot image past 24 hours from subreddit. params: name of subreddit, [amount of images]'
        });
    }

    async run(message, args) {
        let subreddit = read_arguments.readSubreddit(args);
        let amountOfPictures = read_arguments.readAmountOfPictures(message, args);
        if(subreddit != false) {
            if(amountOfPictures != false) {
                read_json.read_jsonCorrect(message, subreddit, amountOfPictures, '/hot.json');
            }
        } else {
            message.reply("Please enter a subreddit!");
        }
    }
}



module.exports = getHotImageFromReddit;