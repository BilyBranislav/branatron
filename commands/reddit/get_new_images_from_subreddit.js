const commando = require('discord.js-commando');
const read_json = require('read_json.js');
const read_arguments = require('read_arguments');

class getNewImageFromReddit extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_reddit_new',
            group: 'reddit',
            memberName: 'bruh_reddit_new',
            description: 'Gets new image past 24 hours from subreddit. params: name of subreddit, [amount of images]'
        });
    }

    async run(message, args) {
        let subreddit = read_arguments.readSubreddit(args);
        let amountOfPictures = read_arguments.readAmountOfPictures(message, args);
        if(subreddit != false) {
            if(amountOfPictures != false) {
                read_json.read_jsonCorrect(message, subreddit, amountOfPictures, '/top.json');
            }
        } else {
            message.reply("Please enter a subreddit!");
        }
    }
}

module.exports = getNewImageFromReddit;