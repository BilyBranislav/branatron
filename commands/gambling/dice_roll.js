const commando = require('discord.js-commando');
const read_arguments = require('read_arguments.js');
const dbWrite = require('write_to_database.js');
const sqlite3 = require('sqlite3').verbose();

class DiceRollCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_dice',
            group: 'gambling',
            memberName: 'bruh_dice',
            description: 'Rolls a six sided dice. params: face'
        });
    }

    async run(message, args) {
        let sidesOfDice = 6;
        let input = read_arguments.readTwoNumbers(args, message, sidesOfDice);
        if(input != null) {
            let credit = 0;
            let db = new sqlite3.Database('././database/credit.db', sqlite3.OPEN_READWRITE, (err) => {
                if (err) {
                    console.error(err.message);
                }   
            });
            let id = message.author.id;
            let sql = 'select credit from credit where id ==?';
            db.get(sql, id, (err, row) => {
                if(err) {
                  return console.log(err.message);
                }
                credit = row.credit
                let diceRoll = Math.floor(Math.random() * sidesOfDice + 1);
                if(input[1] > credit) {
                    message.reply("You do not have enough credit to gamble!");
                } else if(input[0] == diceRoll) {
                    message.reply("You were right!");
                    dbWrite.updateCredit((credit + input[1] * 5), message);
                } else {
                    message.reply("You were wrong, dice landed on " + diceRoll);
                    dbWrite.updateCredit((credit - input[1]), message);
                }  
            })
            db.close((err) => {
                if(err) {
                  console.log(err);
                }
            });
        }
    }
}

module.exports = DiceRollCommand;