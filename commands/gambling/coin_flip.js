const commando = require('discord.js-commando');
const validator = require('validator');
const read_arguments = require('read_arguments.js');
const dbWrite = require('write_to_database.js');
const sqlite3 = require('sqlite3').verbose();

class CoinFlipCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_flip',
            group: 'gambling',
            memberName: 'bruh_flip',
            description: 'Flips a coin. What do you expect. params: heads or tails'
        });
    }

    async run(message, args) {
        args = args.toLowerCase();
        let input = read_arguments.readTwoNumbers(args, message);
        console.log(input[0] + " " + input[1]);
        if(input[0]  == 'heads' || input[0] == 'tails') {
            let credit = 0;
            let db = new sqlite3.Database('././database/credit.db', sqlite3.OPEN_READWRITE, (err) => {
                if (err) {
                    console.error(err.message);
                }   
            });
            let id = message.author.id;
            let sql = 'select credit from credit where id ==?';
            db.get(sql, id, (err, row) => {
                if(err) {
                  return console.log(err.message);
                }
                credit = row.credit;
                let coinFlip = Math.floor(Math.random() * 2);
                if(input[1] > credit) {
                    message.reply("You do not have enough credit to gamble!");
                } else if (coinFlip == 0 && input[0] == 'tails') {
                    message.reply('You were right! Coin landed on tails!');
                    credit = credit + parseInt(input[1]);
                    dbWrite.updateCredit(credit, message);
                } else if (coinFlip == 1 && input[0] == 'heads') {
                    message.reply('You were right! Coin landed on heads!');
                    credit = credit + parseInt(input[1]);
                    dbWrite.updateCredit(credit, message);
                } else {
                    message.reply("Coin did not land on " + input[0]);
                    credit = credit - parseInt(input[1]);
                    dbWrite.updateCredit(credit, message);
                }
            })
            db.close((err) => {
                if(err) {
                  console.log(err);
                }
            });
        } else {
            message.reply('Invalid input! Please enter either heads or tails');
        }
    }
}

module.exports = CoinFlipCommand;