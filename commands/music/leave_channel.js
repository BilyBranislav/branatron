const commando = require('discord.js-commando');

class LeaveChannelCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_leave',
            group: 'music',
            memberName: 'bruh_leave',
            description: 'Leaves the voice channel of the commander. params: {}'
        });
    }

    async run(message, args) {
        if(message.guild.voiceConnection) {
            message.guild.voiceConnection.disconnect();
        } else {
            message.reply("I need to be in a voice channel to leave!");
        }
    }
}

module.exports = LeaveChannelCommand;