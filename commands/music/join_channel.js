const commando = require('discord.js-commando');
const validator = require('validator');

class JoinChannelCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_join',
            group: 'music',
            memberName: 'bruh_join',
            description: 'Joins the voice channel of the commander. params: {}'
        });
    }

    async run(message, args) {
        if(message.member.voiceChannel) {
            if(!message.guild.voiceConnection) {
                message.member.voiceChannel.join()
                .then(connection => {
                    message.reply("Succesfully joined!")
                });
            }
        } else {
            message.reply("You must be in the voice channel");
        }
    }
}

module.exports = JoinChannelCommand;