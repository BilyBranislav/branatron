const commando = require('discord.js-commando');
const sqlite3 = require('sqlite3');
const write_database = require('write_to_database');

class showCredit extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'bruh_credit',
            group: 'economy',
            memberName: 'bruh_credit',
            description: 'Shows amount of credit'
        });
    }

    async run(message, args) {
        let taggedUser = args;
        let id;
        if(taggedUser != '') {
            id = taggedUser;
        } else {
            id = message.author.id;
        }
        id = id.replace("<", "");
        id = id.replace(">", "");
        id = id.replace("@", "");
        console.log(id);
        let db = new sqlite3.Database('././database/credit.db', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                return message.reply("Please, tag a real user in this channel");
            }
        });

        let sql = 'select credit from credit where id ==?';
        db.get(sql, id, (err, row) => {
            if(err) {
                message.reply("Please, tag a real user in this channel");
                return console.log(err.message);
            }
            if(row == undefined) {
                write_database.insertNewUser(id, message);
            } else if(id == message.author.id) {
                message.reply("Your credit is " + row.credit);
            } else {
                message.guild.fetchMember(id).then(member => {
                    message.reply(member +"'s credit is " + row.credit);
                })
            }
        });
        db.close((err) => {
            if(err) {
                return console.log(err);
            }
        });
    }
}

module.exports = showCredit;